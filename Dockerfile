FROM python:3.6
ENV PYTHONUNBUFFERED 1
ADD src /src
WORKDIR /src
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile
